<?php

use Illuminate\Support\Facades\Artisan;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/migrate', function () {
	Artisan::call('migrate');
	return 'Migration success';
});

Route::get('/seed', function () {
	Artisan::call('db:seed');
	return 'Database seeder run successfully';
});

Route::get('/passport-install', function () {
	//Artisan::call('passport:install');
	shell_exec('php ../artisan passport:install');
	return 'Passport installed successfully';
});

