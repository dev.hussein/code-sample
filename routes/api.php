<?php
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
*/

Route::group(['prefix' => 'v1', 'namespace' => 'API\V1'], function () {
	/**
	 * Authentication
	 */
	Route::post('register', 'AuthController@register');
    Route::post('login', 'AuthController@login');
    Route::post('verify', 'AuthController@verify')->middleware('auth:api');
    Route::post('resend', 'AuthController@resend')->middleware('auth:api');
    Route::get('logout', 'AuthController@logout')->middleware('auth:api');
});

Route::group(['prefix' => 'v1', 'namespace' => 'API\V1', 'middleware' => ['auth:api', 'verified.phone']], function () {  

	Route::get('categories', 'CategoryController@index');
});