<?php

// Order statuses
define('ORDER_STATUS_NEW', 0);
define('ORDER_STATUS_CANCELLED', 1);
define('ORDER_STATUS_COMPLETED', 2);