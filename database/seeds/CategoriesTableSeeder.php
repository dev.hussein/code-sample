<?php

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = Category::all();

        if (count($categories) == 0) {
        	$category = new Category;
        	$category->name = 'خضروات';
        	$category->image = 'uploads/categories/vegetables.png';
        	$category->save();

        	$category = new Category;
        	$category->name = 'بقالة';
        	$category->image = 'uploads/categories/grocery.png';
        	$category->save();

        	$category = new Category;
        	$category->name = 'محمصة';
        	$category->image = 'uploads/categories/toaster.png';
        	$category->save();

        	$category = new Category;
        	$category->name = 'صيدلية';
        	$category->image = 'uploads/categories/pharmacy.png';
        	$category->save();

        	$category = new Category;
        	$category->name = 'كماليات';
        	$category->image = 'uploads/categories/accessories.png';
        	$category->save();

        	$category = new Category;
        	$category->name = 'أخرى';
        	$category->image = 'uploads/categories/other.png';
        	$category->save();
        }
    }
}
