<?php

namespace App\Contracts;

interface CategoryRepositoryContract
{
	public function getAll();
}