<?php

namespace App\Contracts;

interface UserRepositoryContract
{
	public function register($data);
}