<?php

namespace App\Http\Controllers\API\V1;

use App\Contracts\UserRepositoryContract;
use App\Http\Resources\User as UserResource;
use App\Http\Requests\Auth\RegisterRequest;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\VerifyRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends APIController
{
    /**
     * Register new user
     * @param  RegisterRequest $request 
     * @return mixed
     */
    public function register(RegisterRequest $request, UserRepositoryContract $userRepo)
    {
    	$user = $userRepo->register($request->validated());

    	//send verification code sms

    	return $this->respondCreated(new UserResource($user));
    }

    /**
     * Login
     * @param  LoginRequest $request 
     * @return mixed
     */
    public function login(LoginRequest $request)
    {
        if (! Auth::attempt($request->only('phone', 'password'))) {
            return $this->errorUnauthorized();
        }

        $user = Auth::user(); 

        if (! $user->status) {
            return $this->errorWrongArgs('Your account has been blocked');
        }

		return $this->respond(new UserResource($user));        
    }

    /**
     * Verify user phone
     * @param  VerifyRequest $request 
     * @return mixed
     */
    public function verify(VerifyRequest $request)
    {
    	$user = $request->user();

    	if ($user->verified) {
    		return $this->errorWrongArgs('Your account already verified');
    	}

    	if ($user->verification_code != $request->verification_code) {
    		return $this->errorWrongArgs('Verification code is wrong');
    	}

    	$user->verify();

    	return $this->respond(new UserResource($user)); 
    }

    /**
     * Resend verification code
     * @return mixed
     */
    public function resend()
    {
    	//send verification code sms
    	
    	return $this->respondWithMessage('Verification code sent successfully');
    }

    /**
     * Logout
     * @return mixed
     */
    public function logout()
    {
    	auth()->user()->token()->revoke();

    	return $this->respondWithMessage('User successfully logout');
    }

}
