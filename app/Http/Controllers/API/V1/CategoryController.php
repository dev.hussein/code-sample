<?php

namespace App\Http\Controllers\API\V1;

use App\Contracts\CategoryRepositoryContract;
use App\Http\Resources\Category as CategoryResource;
use Illuminate\Http\Request;

class CategoryController extends APIController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(CategoryRepositoryContract $categoryRepo)
    {
        return $this->respond(CategoryResource::collection($categoryRepo->getAll()));
    }
}
