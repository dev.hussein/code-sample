<?php

namespace App\Http\Requests\Orders;

use Illuminate\Validation\Rule;
use App\Http\Requests\API\FormRequest;

class StoreOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'quantity' => 'required|numeric',
            'category_id' => ['required','integer', Rule::exists('categories' , 'id')->where(function ($query) {
                    $query->where('status', 1);
                })]
        ];
    }
}
