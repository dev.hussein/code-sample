<?php

namespace App\Http\Middleware;

use Closure;

class EnsurePhoneIsVerified
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (! $request->user()->verified) {
            return response()->json([
                'error' => [
                    'code' => 'GEN-FUBARGS',
                    'http_code' => 400,
                    'message' => 'You must verify your account'
                ]
            ], 400);
        }

        return $next($request);
    }
}
