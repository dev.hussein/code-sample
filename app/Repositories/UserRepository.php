<?php 

namespace App\Repositories;

use App\Contracts\UserRepositoryContract;
use App\Models\User;

class UserRepository implements UserRepositoryContract
{
	/**
	 * Saves the resource in the database
	 * @param  object $data 
	 * @return App\Models\User
	 */
	public function register($data)
	{
		$user = new User;
		$user->name = $data['name'];
		$user->phone = $data['phone'];
		$user->password = bcrypt($data['password']);
		$user->verification_code = mt_rand(1000, 9999);
		$user->save();

		return $user;
	}
}