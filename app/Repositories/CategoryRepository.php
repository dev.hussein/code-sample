<?php 

namespace App\Repositories;

use App\Contracts\CategoryRepositoryContract;
use App\Models\Category;

class CategoryRepository implements CategoryRepositoryContract
{
	public function getAll()
	{
		$categories = Category::active()->get();

		return $categories;
	}
}